name := "Exercise Log"

version := "0.1"

scalaVersion := "2.13.5"

scalacOptions := Seq("-unchecked", "-deprecation")

Compile / run / fork := true //https://www.scala-sbt.org/1.x/docs/Forking.html#Enable+forking
run / connectInput := true //https://www.scala-sbt.org/1.x/docs/Forking.html#Configuring+Input
outputStrategy := Some(StdoutOutput) //https://www.scala-sbt.org/1.x/docs/Forking.html#Configuring+output

libraryDependencies ++= Seq(
    "com.github.finagle" %% "finchx-core" % "0.32.1",
    "com.github.finagle" %% "finchx-circe" % "0.32.1",
    "io.circe" %% "circe-generic" % "0.13.0"
)

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.15.2" % "test"

libraryDependencies += "com.outr" %% "scribe" % "3.5.3"
libraryDependencies += "com.outr" %% "scribe-file" % "3.5.3"

dependencyOverrides += "org.typelevel" %% "cats-core" % "2.0.0"
dependencyOverrides += "org.typelevel" %% "jawn-parser" % "1.0.0-RC3"
dependencyOverrides += "io.circe" %% "circe-core" % "0.13.0-M2"
dependencyOverrides += "io.circe" %% "circe-jawn" % "0.13.0-M2"

import java.time.LocalDateTime
import org.scalacheck._
import Arbitrary.arbitrary
import cats.data.NonEmptyVector
import domain._

object WorkoutSpecification extends Properties("Workout") {
  import Prop.{forAll, propBoolean}

  property("starts") = forAll(genExercise, genIntensity, genLocalDateTime) {
    (exercise: Exercise, intensity: Intensity, utcStartedAt: LocalDateTime) =>
    val (workout, event) = Workout.start(exercise, intensity, utcStartedAt)
    val actualSet = workout.ongoing.ongoing
    ("no idle executions" |: workout.finishedExecutions.isEmpty) &&
    ("ongoing execution" |: {
      ("no finished sets" |: workout.ongoing.finishedSets.isEmpty) &&
      ("started set" |: {
        ("specified exercise" |: actualSet.exercise == exercise) &&
        ("specified intensity" |: actualSet.intensity == intensity) &&
        ("specified start date" |: actualSet.utcStartedAt == utcStartedAt)
      })
    }) &&
    ("workout started event" |: {
      ("started set" |: event.startedSet == actualSet)
    })
  }

  property("starts execution") = forAll(genIdleWorkout, genExercise, genIntensity, genLocalDateTime) {
    (wk: IdleWorkout, exercise: Exercise, intensity: Intensity, utcStartedAt: LocalDateTime) =>

    val (actualWorkout, actualEvent) = wk.startExecution(exercise, intensity, utcStartedAt)
    val previousFinishedExecs = Set.from(wk.finished)
    val newFinishedExecs = Set.from(actualWorkout.finishedExecutions)
    val execsFromNewOnly = newFinishedExecs &~ previousFinishedExecs
    val actualSet = actualWorkout.ongoing.ongoing
    ("one new finished exec" |: execsFromNewOnly.size == 1) &&
    ("new finished exec" |: {
      execsFromNewOnly.headOption map { actualExec =>
        ("finished sets from previous idle" |: wk.idle.sets == actualExec.sets)
      } getOrElse { propBoolean(false) }
    }) &&
    ("new exec no finished sets" |: actualWorkout.ongoing.finishedSets.isEmpty) &&
    ("started set" |: {
      ("specified exercise" |: exercise == actualSet.exercise) &&
      ("specified intensity" |: intensity == actualSet.intensity) &&
      ("specified start date" |: utcStartedAt == actualSet.utcStartedAt)
    }) &&
    ("started execution event" |: {
      ("started execution" |: actualWorkout.ongoing == actualEvent.startedExecution)
    })
  }

  property("starts set") = forAll(genIdleWorkout, genExercise, genIntensity, genLocalDateTime) {
    (wk: IdleWorkout, exercise: Exercise, intensity: Intensity, utcStartedAt: LocalDateTime) =>

    val (actualWorkout, actualEvent) = wk.startSet(exercise, intensity, utcStartedAt)
    val actualSet = actualWorkout.ongoing.ongoing
    ("finished execs match" |: wk.finished.toVector == actualWorkout.finishedExecutions) &&
    ("finished sets match" |: wk.idle.sets.toVector == actualWorkout.ongoing.finishedSets) &&
    ("started set" |: {
      ("specified exercise" |: exercise == actualSet.exercise) &&
      ("specified intensity" |: intensity == actualSet.intensity) &&
      ("specified start date" |: utcStartedAt == actualSet.utcStartedAt)
    }) &&
    ("started set event" |: {
      ("started set" |: actualSet == actualEvent.startedSet)
    })
  }

  property("finishes started set") = forAll(genOngoingWorkout, genReps) { (wk: OngoingWorkout, repCount: Reps) =>
    val startedAt = wk.ongoing.ongoing.utcStartedAt
    val finishedAt = startedAt.plusSeconds(Gen.choose(10, 30).sample.get)
    val (updated, event) = wk.finishSet(repCount, finishedAt)

    val previousFinishedSets = Set.from(wk.ongoing.finishedSets)
    val newFinishedSets = Set.from(updated.idle.sets.toVector)
    val finishedSetsOnlyFromNew = newFinishedSets &~ previousFinishedSets

    ("same finished executions" |: wk.finishedExecutions == updated.finished.toVector) &&
    ("one finished set only from new" |: finishedSetsOnlyFromNew.size == 1) &&
    ("finished set from new" |: {
      val previousSet = wk.ongoing.ongoing
      finishedSetsOnlyFromNew.headOption map { actualSet =>
        ("same exercise" |: previousSet.exercise == actualSet.exercise) &&
        ("same intensity" |: previousSet.intensity == actualSet.intensity) &&
        ("same start date" |: previousSet.utcStartedAt == actualSet.utcStartedAt) &&
        ("specified rep count" |: repCount == actualSet.repCount) &&
        ("specified finish date" |: finishedAt == actualSet.utcFinishedAt) &&
        ("in event" |: actualSet == event.finishedSet)
      } getOrElse { propBoolean(false) }
    })
  }

  private def genOngoingWorkout: Gen[OngoingWorkout] = for {
    finishedSize <- Gen.choose(0, 9)
    finishedExecs <- Gen.containerOfN[Vector, IdleExecution](finishedSize, genIdleExecution)
    ongoing <- genOngoingExecution
  } yield OngoingWorkout.create(finishedExecs, ongoing)
  private def genIdleWorkout: Gen[IdleWorkout] = for {
    finishedSize <- Gen.choose(0, 9)
    finished <- Gen.containerOfN[Vector, IdleExecution](finishedSize, genIdleExecution)
    idle <- genIdleExecution
  } yield IdleWorkout(finished, idle)
  private def genOngoingExecution: Gen[OngoingExecution] = for {
    started <- genStartedSet
    finishedCount <- Gen.choose(0, 9)
    finishedSets <- Gen.containerOfN[Vector, FinishedSet](finishedCount, genFinishedSet)
  } yield OngoingExecution(finishedSets, started)
  private def genIdleExecution: Gen[IdleExecution] = for {
    head <- genFinishedSet
    tailSize <- Gen.choose(0, 9)
    tail <- Gen.containerOfN[Vector, FinishedSet](tailSize, genFinishedSet)
    es = NonEmptyVector(head, tail)
  } yield IdleExecution(es)
  private def genStartedSet: Gen[StartedSet] = for {
    exercise <- genExercise
    intensity <- genIntensity
    utcStartedAt <- genLocalDateTime
  } yield StartedSet(exercise, intensity, utcStartedAt)
  private def genFinishedSet: Gen[FinishedSet] = for {
    exercise <- genExercise
    intensity <- genIntensity
    utcStartedAt <- genLocalDateTime
    repCount <- genReps
    utcFinishedAt <- genUtcFinishedAt(utcStartedAt)
  } yield FinishedSet(exercise, intensity, utcStartedAt, repCount, utcFinishedAt)
  private def genExercise: Gen[Exercise] = arbitrary[String].map(Exercise)
  private def genIntensity: Gen[Intensity] = Gen.oneOf(Gen.lzy(genProgression), Gen.lzy(genWeight))
  private def genProgression: Gen[Progression] = arbitrary[Int].map(Progression)
  private def genWeight: Gen[Weight] = arbitrary[Float].map(Weight)
  private def genReps: Gen[Reps] = arbitrary[Int].map(Reps)
  private def genUtcFinishedAt(utcStartedAt: LocalDateTime): Gen[LocalDateTime] =
    genLocalDateTime suchThat { utcStartedAt.compareTo(_) < 0 }
  private def genLocalDateTime: Gen[LocalDateTime] = for {
    year <- Gen.choose(1990, 2020)
    month <- Gen.choose(1, 12)
    day <- Gen.choose(1, 28)
    hour <- Gen.choose(0, 23)
    minute <- Gen.choose(0, 59)
    second <- Gen.choose(0, 59)
    nano <- Gen.choose(0, 999999999)
  } yield LocalDateTime.of(year, month, day, hour, minute, second, nano)
}
import scala.util.chaining._

import com.twitter.finagle.{Http, Service}
import com.twitter.finagle.http.{Request, Response}
import com.twitter.util.Await

import io.finch._, cats.effect.IO
import io.finch.circe._
import io.circe._
import io.circe.generic.auto._
import io.circe.generic.semiauto._
import io.circe.syntax._

import scribe.file._

import domain._
import cats.effect.IOApp
import cats.effect.ExitCode
import cats.effect.ExitCase
import cats.syntax.functor._
import java.time.LocalDateTime
import java.time.ZoneOffset
import com.twitter.io.Buf

//https://typelevel.org/cats-effect/docs/2.x/datatypes/ioapp#cancelation-and-safe-resource-release
object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = for {
    _ <- IO(setupLogging())
    service = App.toService()
    //https://typelevel.org/cats-effect/docs/2.x/datatypes/io#safe-resource-acquisition-and-release
    _ <- IO(Http.server.serve(":8080", service)).bracketCase { server =>
      IO(Await.ready(server))
    } {
      case (_, ExitCase.Completed | ExitCase.Error(_)) => IO.unit
      case (server, ExitCase.Canceled) => {
        val closing = server.close()
        for {
          _ <- IO(println("Closing server..."))
          _ <- IO.async { (cb: Either[Throwable, Unit] => Unit) =>
            closing
              .onSuccess(_ => cb(Right(())))
              .onFailure(e => cb(Left(e)))
          }
          _ <- IO(println("Server closed"))
        } yield ()
      }
    }
  } yield ExitCode.Success

  private def setupLogging() = {
    scribe.Logger.root
      .withHandler(writer = FileWriter("logs" / ("app-" % year % "-" % month % "-" % day % ".log")))
      .replace()
  }
}

object App extends Endpoint.Module[IO] {
  import GenericDerivation._

  case class Locale(language: String, country: String)
  case class Time(locale: Locale, time: String)
  //https://circe.github.io/circe/codecs/adt.html
  case class WorkoutStart(exercise: String, intensity: Intensity)

  private def currentTime(l: java.util.Locale): String =
    java.util.Calendar.getInstance(l).getTime.toString

  private val time: Endpoint[IO, Time] =
    post("time" :: jsonBody[Locale]) { l: Locale =>
      val ct = new java.util.Locale(l.language, l.country).pipe(currentTime)
      Time(l, ct).pipe(Ok)
    }

  private val hello: Endpoint[IO, String] =
    get("hello") {
      Ok("Hello, World!")
    }

  private val startWorkout: Endpoint[IO, Unit] =
    post("workouts" :: jsonBody[WorkoutStart]) { start: WorkoutStart =>
      val exercise = Exercise(start.exercise)
      val now = LocalDateTime.now(ZoneOffset.UTC)
      Workout.start(exercise, start.intensity, now)
      NoContent[Unit]
    }

  private val logging: Endpoint.Compiled[IO] => Endpoint.Compiled[IO] = compiled =>
    compiled.tapWithF { (req, res) => for {
        _ <- IO(scribe.info(s"Request: $req"))
        _ <- IO(scribe.info(s"Response: $res"))
      } yield res
    }

  def toService() : Service[Request, Response] = {
    val compiled = logging(
      Bootstrap
      .serve[Application.Json](time :+: startWorkout)
      .serve[Text.Plain](hello)
      .compile
    )
    Endpoint.toService(compiled)
  }
}

object GenericDerivation {
  implicit val encodeIntensity: Encoder[Intensity] = Encoder.instance {
    case p: Progression => p.asJson
    case w: Weight => w.asJson
  }

  implicit val decodeIntensity: Decoder[Intensity] =
    List[Decoder[Intensity]](
      Decoder[Progression].widen,
      Decoder[Weight].widen
    ).reduceLeft(_ or _)
}
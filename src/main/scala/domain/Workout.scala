package domain

import java.time.LocalDateTime
import cats.data.NonEmptyVector

sealed trait Workout
object Workout {

  def start(exercise: Exercise, intensity: Intensity, utcStartedAt: LocalDateTime): (OngoingWorkout, WorkoutStarted) = {
    require(exercise != null, "exercise")
    require(intensity != null, "intensity")
    require(utcStartedAt != null, "utcStartedAt")

    val set = StartedSet(exercise, intensity, utcStartedAt)
    var exec = OngoingExecution(Vector.empty[FinishedSet], set)
    val event = WorkoutStarted(set)
    val workout = OngoingWorkout.create(Vector.empty[IdleExecution], exec)
    (workout, event)
  }

  sealed trait Event
  case class WorkoutStarted(startedSet: StartedSet) extends Event
  case class ExecutionStarted(startedExecution: OngoingExecution) extends Event
  case class SetStarted(startedSet: StartedSet) extends Event
  case class SetFinished(finishedSet: FinishedSet) extends Event
}

sealed abstract case class OngoingWorkout(finishedExecutions: Vector[IdleExecution], ongoing: OngoingExecution)
extends Workout {
  def finishSet(repCount: Reps, utcFinishedAt: LocalDateTime): (IdleWorkout, Workout.SetFinished) = {

    require(repCount != null, "repCount")
    require(utcFinishedAt != null, "utcFinishedAt")

    val StartedSet(exercise, intensity, utcStartedAt) = ongoing.ongoing

    val finishedSet = FinishedSet(exercise, intensity, utcStartedAt, repCount, utcFinishedAt)
    val newSets = NonEmptyVector.fromVectorUnsafe(ongoing.finishedSets :+ finishedSet)
    val newExec = IdleExecution(newSets)

    val newWorkout = IdleWorkout(finishedExecutions, newExec)
    val event = Workout.SetFinished(finishedSet)
    (newWorkout, event)
  }
}
object OngoingWorkout {
  def create(finishedExecutions: Vector[IdleExecution], ongoing: OngoingExecution): OngoingWorkout = {
    require(finishedExecutions != null, "finishedExecutions")
    require(ongoing != null, "ongoing")
    new OngoingWorkout(finishedExecutions, ongoing) {}
  }

  sealed trait Error
  case class SetNotFound(utcSetStartedAt: LocalDateTime) extends Error
}

case class IdleWorkout(finished: Vector[IdleExecution], idle: IdleExecution) extends Workout {

  def startExecution(exercise: Exercise, intensity: Intensity, utcStartedAt: LocalDateTime):
  (OngoingWorkout, Workout.ExecutionStarted) = {

    require(exercise != null, "exercise")
    require(intensity != null, "intensity")
    require(utcStartedAt != null, "utcStartedAt")

    val started = StartedSet(exercise, intensity, utcStartedAt)
    val newExec = OngoingExecution(Vector(), started)
    val newWorkout = new OngoingWorkout(finished.toVector :+ idle, newExec) {}
    val event = Workout.ExecutionStarted(newExec)
    (newWorkout, event)
  }

  def startSet(exercise: Exercise, intensity: Intensity, utcStartedAt: LocalDateTime):
  (OngoingWorkout, Workout.SetStarted) = {

    require(exercise != null, "exercise")
    require(intensity != null, "intensity")
    require(utcStartedAt != null, "utcStartedAt")

    val started = StartedSet(exercise, intensity, utcStartedAt)
    val newExec = OngoingExecution(idle.sets.toVector, started)
    val newWorkout = new OngoingWorkout(finished.toVector, newExec) {}
    val event = Workout.SetStarted(started)
    (newWorkout, event)
  }
}
case class FinishedWorkout(executions: NonEmptyVector[IdleExecution], utcFinishedAt: LocalDateTime) extends Workout

//better name?
sealed trait Execution
case class OngoingExecution(finishedSets: Vector[FinishedSet], ongoing: StartedSet) extends Execution
case class IdleExecution(sets: NonEmptyVector[FinishedSet])  extends Execution

sealed trait Set
case class StartedSet(exercise: Exercise, intensity: Intensity, utcStartedAt: LocalDateTime) extends Set
case class FinishedSet(exercise: Exercise, intensity: Intensity, utcStartedAt: LocalDateTime, repCount: Reps,
                        utcFinishedAt: LocalDateTime) extends Set

case class Reps(count: Int)

sealed trait Intensity
case class Progression(value: Int) extends Intensity
case class Weight(valueInKg: Float) extends Intensity

case class Exercise(name: String)
